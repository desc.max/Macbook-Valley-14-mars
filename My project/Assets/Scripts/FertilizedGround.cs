using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FertilizedGround : MonoBehaviour
{

    public SpriteRenderer ground;
    public Sprite greenGround, yellowGround;
    public GameObject orange, carrot, courgette, tomate, peach, radish, strawberry, apple, Zucchini;
    public Fruits orangeScript, carrotScript, courgetteScript, tomateScript, peachScript, radishScript, strawberryScript, appleScript, ZucchiniScript;


    public GameObject pressE;
    public GameObject shopMenu;
    private Transform player;
    public float minimumRadiusInteraction = 1;

    private bool isFree;
    // Start is called before the first frame update
    void Start()
    {
        isFree = true;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        pressE.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) <= minimumRadiusInteraction && isFree) 
        {
            pressE.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                shopMenu.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                StartCoroutine(Popfruit(carrotScript));
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                StartCoroutine(Popfruit(appleScript));
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                StartCoroutine(Popfruit(orangeScript));
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                StartCoroutine(Popfruit(ZucchiniScript));
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                StartCoroutine(Popfruit(peachScript));
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                StartCoroutine(Popfruit(strawberryScript));
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                StartCoroutine(Popfruit(appleScript));
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                StartCoroutine(Popfruit(radishScript));
            }



        }
        else {
            shopMenu.SetActive(false);
            pressE.SetActive(false);
        }
       
    }

    IEnumerator Popfruit(Fruits fruittopop)
    {

        ground.sprite = greenGround;
        isFree = false;
        yield return new WaitForSeconds(fruittopop.GrowTimer);
        

        
        GameObject selectedPrefab = null;

        switch (fruittopop.Fname) 
        {
            case nameFruit.carrot:
               
                selectedPrefab = carrot;
                break;

            case nameFruit.courgette:
              
                selectedPrefab = courgette;
                break;

            case nameFruit.orange:
                
                selectedPrefab = orange;
                break;

            case nameFruit.tomate:
          
                selectedPrefab = tomate;
                break;

            case nameFruit.peach:
           
                selectedPrefab = carrot;
                break;

            case nameFruit.radish:
               
                selectedPrefab = radish;
                break;

            case nameFruit.strawberry:
               
                selectedPrefab = strawberry;
                break;

            case nameFruit.apple:
             
                selectedPrefab = apple;
                break;

            case nameFruit.Zucchini:
      
                selectedPrefab = Zucchini;
                break;
        }
        
   
        Instantiate(selectedPrefab, transform.position, transform.rotation);
        isFree = true;
        ground.sprite = yellowGround;

    }

}

