using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water : MonoBehaviour
{

    bool IsCaryingWater;
    GameObject WaterWell;
    private GameObject player;

    

    // Start is called before the first frame update
    void Start()
    {
        WaterWell = GameObject.FindWithTag("waterwell");
        player = GameObject.FindWithTag("Player");
        IsCaryingWater = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsCaryingWater)
        {
            transform.position = player.transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            IsCaryingWater = true;
        }
        if (collision.gameObject.tag == "Legume")
        {
            IsCaryingWater = false;
            transform.position = WaterWell.transform.position;
        }
    }
}
