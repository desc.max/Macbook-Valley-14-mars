using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HarvesterPanel : MonoBehaviour
{
    
    public Inventaire inv;
    public Harvester har;

    public Transform CarrotSeed, TomatoSeed, OrangeSeed, ZuddichiSeed, RadishSeed, PeachSeed, StrawberrySeed, AppleSeed;
    public Text Carrot, Tomato, Orange, Zuddichi, Radish, Peach, strawberry, apple;
    // Start is called before the first frame update
    void Start()
    {

    }


    void MinusCarrot()
    {
       
        Transform newFruit = Instantiate(CarrotSeed, transform.position, transform.rotation);

    }
    void MinusTomato()
    {
        
        Transform newFruit = Instantiate(TomatoSeed, transform.position, transform.rotation);
    }
    void MinusOrange()
    {
        
        Transform newFruit = Instantiate(OrangeSeed, transform.position, transform.rotation);
    }
    void MinusZuddichi()
    {
       
        Transform newFruit = Instantiate(ZuddichiSeed, transform.position, transform.rotation);
    }
    void MinusPeach()
    {
        Transform newFruit = Instantiate(PeachSeed, transform.position, transform.rotation);
    }

    void QuitMarket()
    {
        gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
