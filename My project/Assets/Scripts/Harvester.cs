using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Harvester : MonoBehaviour
{
    bool CanHarvest = false;
    public GameObject Press_E;
    public GameObject FruitsPanel;

    bool PanelSeed = false;

    public Transform CarrotSeed, TomatoSeed, OrangeSeed, ZuddichiSeed, RadishSeed, PeachSeed, StrawberrySeed, AppleSeed;
    public PlayerMovement PM;
    // Start is called before the first frame update
    void Start()
    {
        Press_E.SetActive(false);
        FruitsPanel.SetActive(false);
    }

    private void Update()
    {
        if (CanHarvest && (Input.GetKeyDown(KeyCode.E)))
        {
            PanelSeed = true;
            FruitsPanel.SetActive(true);

        }

        

        if ((PanelSeed == true) && (Input.GetKeyDown(KeyCode.W)))
        {
            MinusCarrot();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            MinusTomato();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            MinusOrange();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            MinusZuddichi();
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            MinusPeach();
        }
    }
    // Update is called once per frame 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            CanHarvest = true;
            Press_E.SetActive(true);
        }
    }



        void MinusCarrot()
        {

        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(CarrotSeed, transform.position, transform.rotation);
        
        Destroy(gameObject);

        }
        void MinusTomato()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(TomatoSeed, transform.position, transform.rotation);
        Destroy(gameObject);
    }
        void MinusOrange()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(OrangeSeed, transform.position, transform.rotation);
        Destroy(gameObject);
    }
        void MinusZuddichi()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(ZuddichiSeed, transform.position, transform.rotation);
        Destroy(gameObject);
    }
        void MinusPeach()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(PeachSeed, transform.position, transform.rotation);
        Destroy(gameObject);
    }
        void MinusStrawberry()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(StrawberrySeed, transform.position, transform.rotation);
        Destroy(gameObject);
    }
        void MinusApple()
        {
        FruitsPanel.SetActive(false);
        Transform newFruit = Instantiate(AppleSeed, transform.position, transform.rotation);
        Destroy(gameObject);
        }

    
        private void OnTriggerExit2D(Collider2D collision)
        {
            Press_E.SetActive(false);
            CanHarvest = false;



        }

    } 
