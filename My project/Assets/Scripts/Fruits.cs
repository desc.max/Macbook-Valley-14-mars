using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum nameFruit
{
   orange, carrot, courgette, tomate, peach, radish, strawberry, apple, Zucchini
}
[CreateAssetMenu(fileName = "New Fruit")]
public class Fruits : ScriptableObject
{
    public nameFruit Fname;
    Transform fruits;

    public Sprite Artwork;

    public float GrowTimer;

   
    public float waterReserve;
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

  

    // Update is called once per frame
}
