using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Inventaire : MonoBehaviour
{
    public List<Fruits> ListFruits;
    public int nbCarott, nbTomate, nbZucchini, nbRadish, nbStrawberry, nbApple, nbPeach, nbOrange ;
    public Text tcar, ttomate, tzuc, tradis, tstra, tapple, tpeach, torange;
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Legume") //if player enter in collision with object ''legume''
        { 
            switch(collision.gameObject.GetComponent<ReadSO>().so.Fname) //he recovers the int of each legumes
            {
                case nameFruit.carrot:
                    Debug.Log("Carrot");
                    
                    nbCarott = nbCarott + 1;
                    tcar.text = nbCarott.ToString();
                    break;

                case nameFruit.tomate:
                    Debug.Log("tomate");
                    
                    nbTomate = nbTomate + 1;
                    ttomate.text = nbTomate.ToString();
                    break;

                case nameFruit.Zucchini:
                    Debug.Log("Zucchini");
                    nbZucchini = nbZucchini + 1;
                    tzuc.text = nbZucchini.ToString();
                    break;

                case nameFruit.radish:
                    Debug.Log("radish");
                    nbRadish = nbRadish + 1;
                    tradis.text = nbRadish.ToString();
                    break;

                case nameFruit.strawberry:
                    Debug.Log("strawb");
                    nbStrawberry = nbStrawberry + 1;
                    tstra.text = nbStrawberry.ToString();
                    break;

                case nameFruit.apple:
                    Debug.Log("apple");
                    nbApple = nbApple + 1;
                    tapple.text = nbApple.ToString();
                    break;

                case nameFruit.peach:
                    Debug.Log("peach");
                    nbPeach = nbPeach + 1;
                    tpeach.text = nbPeach.ToString();
                    break;


                case nameFruit.orange:
                    Debug.Log("orange");
                    nbOrange = nbOrange + 1;
                    torange.text = nbOrange.ToString();
                    break;



            }


            ListFruits.Add(collision.gameObject.GetComponent<ReadSO>().so); // Add the Scriptable object into the player's list
            Destroy(collision.gameObject);
        }
    }
}
